﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/Parking/balance
        [HttpGet("balance")]
        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }

        // GET: api/Parking/capacity
        [HttpGet("capacity")]
        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }

        // GET: api/Parking/freePlaces
        [HttpGet("freePlaces")]
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
