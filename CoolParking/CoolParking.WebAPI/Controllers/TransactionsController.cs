﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;

        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/transactions/last
        [HttpGet("last")]
        public TransactionInfo GetLastTransaction()
        {
            return Settings.transactions.Last();
        }

        // GET: api/transactions/all
        [HttpGet("all")]
        public IActionResult GetAllTransaction()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch
            {
                return NotFound();
            }
        }

        // PUT: api/Transactions/5
        [HttpPut("topUpVehicle")]
        public IActionResult Put(string id, [FromBody] decimal sum)
        {
            Regex pattern = new Regex(@"^[A-Z]{2}\-\d{4}\-[A-Z]{2}");
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (!pattern.IsMatch(id) && sum <= 0)
            {
                return BadRequest();
            }
            else if( vehicle == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(vehicle);
            }
        }
    }
}
