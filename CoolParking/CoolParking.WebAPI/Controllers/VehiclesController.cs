﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService parkingService;

        public VehiclesController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/vehicles
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(parkingService.GetVehicles());
        }

        // GET: api/vehicles/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(string id)
        {
            Regex pattern = new Regex(@"^[A-Z]{2}\-\d{4}\-[A-Z]{2}");
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if(vehicle == null)
            {
                return NotFound();
            }
            if (!pattern.IsMatch(id))
            {
                return BadRequest();
            }

            return Ok(vehicle);
        }

        // POST: api/vehicles
        [HttpPost]
        public IActionResult Post([FromBody] Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return Ok(vehicle);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/vehicles/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Regex pattern = new Regex(@"^[A-Z]{2}\-\d{4}\-[A-Z]{2}");
            Vehicle vehicle = parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound();
            }
            else if (!pattern.IsMatch(id))
            {
                return BadRequest();
            }
            else
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            
        }
    }
}
