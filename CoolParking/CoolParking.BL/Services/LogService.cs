﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        private StreamReader FileRead { get; set; }
        private StreamWriter FileWrite { get; set; }

        public LogService(string _logFilePath)
        {
            LogPath = _logFilePath;
        }
        public string Read()
        {
            string text;
            using(FileRead = new StreamReader(LogPath))
            {
                text = FileRead.ReadToEnd();
            }
            return text;
        }

        public void Write(string logInfo)
        {
            using (FileWrite = new StreamWriter(LogPath, true))
            {
                FileWrite?.WriteLine(logInfo);
            }
        }
    }
}