﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    static public class Settings
    {
        public static double parkStartBalance = 0;
        public static double parkCapacity = 10;
        public static int paymentPeriodSec = 5;
        public static int logPeriodSec = 60;
        public static double fineRatio = 2.5;
        public static List<TransactionInfo> transactions = new List<TransactionInfo>();

        public static decimal GetTariff(VehicleType vehicleType)
        {
            switch (vehicleType)
            {
                case VehicleType.PassengerCar: return 2;
                case VehicleType.Truck:  return 5;
                case VehicleType.Bus: return 3.5m;
                case VehicleType.Motorcycle: return 1;
                case 0: return 0;
            }
            return 0;
        }
    }
}