﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex pattern = new Regex(@"^[A-Z]{2}\-\d{4}\-[A-Z]{2}");

            if (pattern.IsMatch(id) && balance > 0)
            {
                Id = id.ToUpper();
                VehicleType = vehicleType;
                Balance = balance;
            }
            else
            {
                throw new ArgumentException();
            }
        }
        static public string GenerateRandomRegistrationPlateNumber()
        {
            Random rnd = new Random();
            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string numbers = "1234567890";
            string firstTwo = new string(Enumerable.Repeat(letters, 2)
                .Select(s => s[rnd.Next(s.Length)]).ToArray());
            string fourNums = new string(Enumerable.Repeat(numbers, 4)
                .Select(s => s[rnd.Next(s.Length)]).ToArray());
            string lastTwo = new string(Enumerable.Repeat(letters, 2)
                .Select(s => s[rnd.Next(s.Length)]).ToArray());
            string id = firstTwo + '-' + fourNums + '-' + lastTwo;
            return id;
        }
    }
}