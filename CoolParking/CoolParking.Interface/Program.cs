﻿using System;
using System.Linq;
using System.Net;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.Interface
{
    class Program
    {
        static private ParkingService parking = new ParkingService();
        static void Main(string[] args)
        {
            for (; ; )
            {
                Console.WriteLine("Choose action:\n" +
                                  "Print parking balance - 1\n" +
                                  "Print free places - 2\n" +
                                  "Print earning - 3\n" +
                                  "Print transactions - 4\n" +
                                  "Print transactions history - 5\n" +
                                  "Print vehicles in parking - 6\n" +
                                  "Put vehicle into parking - 7\n" +
                                  "Remove vehicle from parking - 8\n" +
                                  "Top up vehicle balance - 9");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1: PrintParkingBalance(); break;
                    case 2: PrintFreePlaces(); break;
                    case 3: PrintEarnings(); break;
                    case 4: PrintTransactions(); break;
                    case 5: PrintTransactionHistory(); break;
                    case 6: PrintVehiclesInParking(); break;
                    case 7: PutVehicleInPark(); break;
                    case 8: RemoveVehicleFromPark(); break;
                    case 9: TopUpBalance(); break;
                }
                Console.WriteLine(new string('-',15));
            }
        }

        static private void PrintParkingBalance()
        {
            Console.WriteLine($"Balance: {parking.GetBalance()}");
        }

        static private void PrintFreePlaces()
        {
            Console.WriteLine($"Free {parking.GetFreePlaces()} from {Settings.parkCapacity}");
        }

        static private void PrintEarnings()
        {
            Console.WriteLine($"Earnings: {Settings.transactions.Sum(tr => tr.Sum)}");
        }

        static private void PrintTransactions()
        {
            foreach (TransactionInfo tr in parking.GetLastParkingTransactions())
            {
                Console.Write($"{DateTime.Now.ToLongTimeString()}: ");
                Console.Write($"Vehicle Id: {tr.VehicleId} - ");
                Console.Write($"Earnings: {tr.Sum}");
            }
        }

        static private void PrintTransactionHistory()
        {
            Console.WriteLine($"{parking.ReadFromLog()}");

        }

        static private void PrintVehiclesInParking()
        {
            foreach(Vehicle v in parking.GetVehicles())
            {
                Console.WriteLine($"Vehicle: {v.VehicleType}\n" +
                                  $"ID: {v.Id}\n" +
                                  $"Balance: {v.Balance}");
                Console.WriteLine(new string('_', 10));
            }
        }

        static private void PutVehicleInPark()
        {
            try
            {
                string Id = Vehicle.GenerateRandomRegistrationPlateNumber();
                Console.WriteLine($"     Vehicle Id(XX-yyyy-XX): {Id}");
                Console.Write("     Vehicle Type(bus, moto, truck, car): ");
                string type = Console.ReadLine();
                VehicleType vType;
                switch (type)
                {
                    case "bus": vType = VehicleType.Bus; break;
                    case "moto": vType = VehicleType.Motorcycle; break;
                    case "truck": vType = VehicleType.Truck; break;
                    case "car": vType = VehicleType.PassengerCar; break;
                    default: vType = VehicleType.PassengerCar; break;
                }
                Console.Write("     Vehicle Balance: ");
                decimal balance = decimal.Parse(Console.ReadLine());

                Vehicle vehicle = new Vehicle(Id, vType, balance);

                parking.AddVehicle(vehicle);
                Console.WriteLine($"{vehicle.VehicleType} with Id: {vehicle.Id} with Balance: {vehicle.Balance} has been added to parking.");
            }
            catch
            {
                throw new Exception("Error in creating and adding vehicle.");
            }

        }

        static private void RemoveVehicleFromPark()
        {
            try
            {
                Console.Write("        Write vehicle id to remove it: ");
                string Id = Console.ReadLine();
                parking.RemoveVehicle(Id);
                Console.Write($"        Vehicle with Id: {Id} has been removed.");
            }
            catch
            {
                throw new Exception("Error in removing vehicle.");
            }

        }

        static private void TopUpBalance()
        {
            try
            {
                Console.Write("        Type vehicle id: ");
                string id = Console.ReadLine();
                Console.Write("        Type amount of money to top up: ");
                decimal sum = decimal.Parse(Console.ReadLine());

                parking.TopUpVehicle(id, sum);
                Console.Write($"        Balance of vehicle with Id: {id} has been upped for {sum}$");
            }
            catch
            {
                throw new Exception("Error in typing data.");
            }
        }
    }
}
